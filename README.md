# FSL minimal Docker container example

FSL has historically been a monolithic download and install. As of FSL 6.0.6 all FSL software is organised as conda packages. This allows users to install FSL packages as needed (useful for keeping docker container size small). Users can also get a full install via the fslinstaller.py install script. 

This repository contains a very basic demonstration of creating a minimal docker container that does one job: bet (brain extraction). When you build the container, only FSL's bet tool and its dependencies will be installed. The rest of FSL will be omitted, thus keeping the container size small, and build times short. If you want more FSL programs included you can find their package names here: https://fsl.fmrib.ox.ac.uk/fsldownloads/fslconda/public/

If you have questions please email taylor.hanayik@ndcn.ox.ac.uk

## build

`docker build --tag fsl-bet --platform linux/x86_64 .`

## run

- note: $HOME/Desktop/example_data folder must exist for this example to work.
- note: you can create the example head.nii.gz image via: `cp $FSLDIR/data/standard/MNI152_T1_1mm.nii.gz $HOME/Desktop/example_data/head.nii.gz`
- note: `--platform linux/x86_64` is important if on an M1 mac and using the ARM version of Docker

`docker run -v $HOME/Desktop/example_data:/data -it --platform linux/x86_64 fsl-bet bet /data/head /data/brain`
